import csv


with open('nettfart-2014') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    delays = []
    for row in csv_reader:
        delays.append(int(row[3]))


def calculate_average_delay():
    sum_delay = 0
    for delay in delays:
        sum_delay += delay

    return sum_delay / len(delays)


print(f'Average response time: {calculate_average_delay()}')



