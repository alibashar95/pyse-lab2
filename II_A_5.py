import csv


class FilteredRecord:
    def __init__(self, time_stamp, delay, uplink, downlink):
        self.time_stamp = time_stamp
        self.delay = delay
        self.uplink = uplink
        self.downlink = downlink

    def __str__(self):
        uplink_unit = "Mbit/s"
        if int(self.uplink) > 1000:
            uplink_unit = "kbit/s"
        downlink_unit = "Mbit/s"
        if int(self.downlink) > 1000:
            downlink_unit = "kbit/s"
        return f'{self.time_stamp}   {self.delay} ms, {self.uplink} {uplink_unit}, {self.downlink} {downlink_unit}'


with open('nettfart-2014') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    lines_counted = 0
    records = []
    for row in csv_reader:
        records.append(FilteredRecord(row[4], row[3], row[2], row[1]))
        lines_counted += 1
        if lines_counted == 20:
            break


print('Time stamp            Delay, Uplink,      Downlink')
for record in records:
    print(record)




