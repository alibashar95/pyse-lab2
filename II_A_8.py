import csv


with open('nettfart-mobile.csv', encoding='utf8') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    lines_counted = 0
    count_2g = 0
    count_3g = 0
    count_4g = 0
    count_wlan = 0
    technologies = []
    next(csv_reader)
    for row in csv_reader:
        if row[6] == '2G':
            count_2g += 1
        elif row[6] == '3G':
            count_3g += 1
        elif row[6] == '4G':
            count_4g += 1
        elif row[6] == 'WLAN':
            count_wlan += 1


print(f'Number of 2G: {count_2g}')
print(f'Number of 3G: {count_3g}')
print(f'Number of 4G: {count_4g}')
print(f'Number of WLAN: {count_wlan}')





