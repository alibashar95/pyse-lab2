import csv
import numpy
import matplotlib.pyplot as matplotlib


with open('nettfart-2014') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    delays = []
    for row in csv_reader:
        delays.append(int(row[3]))


x = numpy.sort(delays)
y = numpy.arange(len(delays)) / float(len(delays))
matplotlib.xlabel('x-axis')
matplotlib.ylabel('y-axis')
matplotlib.title('CDF using sorted data set')
matplotlib.plot(x, y, marker='o')
matplotlib.show()


