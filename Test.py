import csv
import statistics as s

import numpy.random

response_times_wired = []
response_times_2g = []
response_times_3g = []
response_times_4g = []
response_times_wlan = []

download_speeds_wired = []
download_speeds_2g = []
download_speeds_3g = []
download_speeds_4g = []
download_speeds_wlan = []

upload_speeds_wired = []
upload_speeds_2g = []
upload_speeds_3g = []
upload_speeds_4g = []
upload_speeds_wlan = []


class FilteredRecord:
    def __init__(self, technology, respons_time, downlink, uplink):
        self.technology = technology
        self.respons_time = float(respons_time)

        # Converting uplink to kbit/s (assuming everything above 1000 is kbit/s)
        self.uplink = float(uplink)
        if self.uplink <= 1000:
            self.uplink = self.uplink*1000

        # Converting downlink to kbit/s (assuming everything above 1000 is kbit/s)
        self.downlink = float(downlink)
        if self.downlink <= 1000:
            self.downlink = self.downlink*1000

        if technology == 'Wired':
            response_times_wired.append(self.respons_time)
            download_speeds_wired.append(self.downlink)
            upload_speeds_wired.append(self.uplink)
        elif technology == '2G':
            response_times_2g.append(self.respons_time)
            download_speeds_2g.append(self.downlink)
            upload_speeds_2g.append(self.uplink)
        elif technology == '3G':
            response_times_3g.append(self.respons_time)
            download_speeds_3g.append(self.downlink)
            upload_speeds_3g.append(self.uplink)
        elif technology == '4G':
            response_times_4g.append(self.respons_time)
            download_speeds_4g.append(self.downlink)
            upload_speeds_4g.append(self.uplink)
        elif technology == 'WLAN':
            response_times_wlan.append(self.respons_time)
            download_speeds_wlan.append(self.downlink)
            upload_speeds_wlan.append(self.uplink)


with open('nettfart-2014') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    records = []
    for row in csv_reader:
        records.append(FilteredRecord('Wired', row[3], row[1], row[2]))

with open('nettfart-mobile.csv', encoding='utf8') as csv_file2:
    csv_reader2 = csv.reader(csv_file2, delimiter=';')
    next(csv_reader2)
    for row in csv_reader2:
        records.append(FilteredRecord(row[6], row[27], row[0], row[1]))


def find_standard_deviation(data):
    return s.stdev(data)


def mean(data):
    return sum(data) / len(data)


print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
print(abs(numpy.random.normal(mean(response_times_wired), find_standard_deviation(response_times_wired))))
