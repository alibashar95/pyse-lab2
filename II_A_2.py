import random


# A function that generates a random number between 2012 and 2020
def generate_rand():
    return random.randrange(2012, 2020)


# Assigning a variable Y to a random number and printing it out
Y = generate_rand()
print('My Y variate is ' + str(Y))



