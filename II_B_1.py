import simpy
import numpy
import random
import csv
import statistics as s


response_times_wired = []
response_times_2g = []
response_times_3g = []
response_times_4g = []
response_times_wlan = []

download_speeds_wired = []
download_speeds_2g = []
download_speeds_3g = []
download_speeds_4g = []
download_speeds_wlan = []

upload_speeds_wired = []
upload_speeds_2g = []
upload_speeds_3g = []
upload_speeds_4g = []
upload_speeds_wlan = []


class FilteredRecord:
    def __init__(self, technology, respons_time, downlink, uplink):
        self.technology = technology
        self.respons_time = float(respons_time)

        # Converting uplink to kbit/s (assuming everything above 1000 is kbit/s)
        self.uplink = float(uplink)
        if self.uplink <= 1000:
            self.uplink = self.uplink*1000

        # Converting downlink to kbit/s (assuming everything above 1000 is kbit/s)
        self.downlink = float(downlink)
        if self.downlink <= 1000:
            self.downlink = self.downlink*1000

        if technology == 'Wired':
            response_times_wired.append(self.respons_time)
            download_speeds_wired.append(self.downlink)
            upload_speeds_wired.append(self.uplink)
        elif technology == '2G':
            response_times_2g.append(self.respons_time)
            download_speeds_2g.append(self.downlink)
            upload_speeds_2g.append(self.uplink)
        elif technology == '3G':
            response_times_3g.append(self.respons_time)
            download_speeds_3g.append(self.downlink)
            upload_speeds_3g.append(self.uplink)
        elif technology == '4G':
            response_times_4g.append(self.respons_time)
            download_speeds_4g.append(self.downlink)
            upload_speeds_4g.append(self.uplink)
        elif technology == 'WLAN':
            response_times_wlan.append(self.respons_time)
            download_speeds_wlan.append(self.downlink)
            upload_speeds_wlan.append(self.uplink)


with open('nettfart-2014') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    records = []
    print('Processing nettfart-2014...')
    for row in csv_reader:
        records.append(FilteredRecord('Wired', row[3], row[1], row[2]))
    print('Finished processing nettfart-2014')

with open('nettfart-mobile.csv', encoding='utf8') as csv_file2:
    csv_reader2 = csv.reader(csv_file2, delimiter=';')
    next(csv_reader2)
    print('Processing nettfart-mobile.csv...')
    for row in csv_reader2:
        records.append(FilteredRecord(row[6], row[27], row[0], row[1]))
    print('Finished processing nettfart-mobile.csv')


def find_standard_deviation(data):
    return s.stdev(data)


def mean(data):
    return sum(data) / len(data)


print('Calculating stdev and mean...')
stdev_response_time = find_standard_deviation(response_times_wired)
mean_response_time = mean(response_times_wired)

stdev_download_speed = find_standard_deviation(download_speeds_wired)
mean_download_speed = mean(download_speeds_wired)

stdev_upload_speed = find_standard_deviation(upload_speeds_wired)
mean_upload_speed = mean(upload_speeds_wired)
print('Finished calculating stdev and mean')


packets_response_times = []
packets_download_speeds = []
packets_upload_speeds = []


def generator(env, probe_name, number_of_packets, network_element_res, routing_vector):
    for i in range(number_of_packets):
        print(f'{env.now}: Generating packet #{i+1}')
        p = packet(env, i+1, routing_vector, network_element_res)
        yield env.process(p)

    probe_response_time = mean(packets_response_times)
    probe_download_speed = mean(packets_download_speeds)
    probe_upload_speed = mean(packets_upload_speeds)

    print(f'Average response time for {probe_name}: {probe_response_time}')
    print(f'Average download speed for {probe_name}: {probe_download_speed}')
    print(f'Average upload speed for {probe_name}: {probe_upload_speed}')


def packet(env, number, routing_vector, network_element_res):
    rv = routing_vector
    time_stamp_start = env.now
    min_download = float('inf')
    min_upload = float('inf')
    going_back = False

    while True:
        for element in rv:
            with network_element_res.request() as request:
                print(f'{env.now}: Packet #{number} waiting for network element {element}')
                # Wait for the network element
                yield request

                print(f'{env.now}: Packet #{number} aquired network element {element}')

                # Checking min upload/download depending on packet direction
                if going_back:
                    upload_speed = abs(numpy.random.normal(mean_upload_speed, stdev_upload_speed))
                    if upload_speed < min_upload:
                        min_upload = upload_speed
                else:
                    download_speed = abs(numpy.random.normal(mean_download_speed, stdev_download_speed))
                    if download_speed < min_download:
                        min_download = download_speed

                # Process time generated from the given data. we devide by 2 times the routing vector to simulate a hop
                process_time = abs(numpy.random.normal(mean_response_time, stdev_response_time)) / (2 * len(rv))
                yield env.timeout(process_time)

                print(f'{env.now}: Network element {element} finished processing packet #{number}')

        if going_back:
            break

        going_back = True
        rv = list(reversed(rv))

    response_time = env.now - time_stamp_start
    print(f'Response time for packet #{number}: {response_time}')
    print(f'Download speed for packet #{number}: {min_download}')
    print(f'Upload speed for packet #{number}: {min_upload}')

    packets_response_times.append(response_time)
    packets_download_speeds.append(min_download)
    packets_upload_speeds.append(min_upload)


random.seed(42)
enviroment = simpy.Environment()

network_element = simpy.Resource(enviroment, capacity=1)
# Generate given number of packets with the given routing_vector (Generating one packet for simplicity)
enviroment.process(generator(enviroment, 'Probe A', 1, network_element, ['B5', 'R5', 'R6', 'NET']))
enviroment.run()
